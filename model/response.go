package model

type ExecuteResponse struct {
	UrlQrcode string `json:"url_qrcode"`
	Url       string `json:"url"`
	ErrCode   int    `json:"errcode"`
	ErrMsg    string `json:"errmsg"`
	Hash      string `json:"hash"`
}

type PayNotifyRequest struct {
	TradeOrderID  string  `form:"trade_order_id" binding:"required"`
	TotalFee      float64 `form:"total_fee" binding:"required"`
	TransactionID string  `form:"transaction_id"`
	OpenOrderID   string  `form:"open_order_id"`
	OrderTitle    string  `form:"order_title"`
	Status        string  `form:"status"`
	Plugins       string  `form:"plugins"`
	Attach        string  `form:"attach"`
	AppID         string  `form:"appid"`
	Time          string  `form:"time"`
	NonceStr      string  `form:"nonce_str"`
	Hash          string  `form:"hash"`
}
